package errors

import (
	"github.com/gin-gonic/gin"
	"log"
)

func GinErrorHandler(gtx *gin.Context, err error, code int) bool {
	if err != nil {
		_ = gtx.Error(err)
		log.Print(err.Error())
		gtx.AbortWithStatusJSON(code, gin.H{"status": false, "message": err.Error()})
		return true
	}
	return false
}
