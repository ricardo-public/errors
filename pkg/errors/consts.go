package errors

import "net/http"

const (
	ErrUnauthorized    = "UNAUTHORIZED"
	ErrForbidden       = "FORBIDDEN"
	ErrDatabaseTimeout = "DATABASE_TIMEOUT"
	ErrNotFound = "NOT_FOUND"
)

var (
	HttpCodes = map[string]int{
		ErrUnauthorized:    http.StatusUnauthorized,
		ErrForbidden:       http.StatusForbidden,
		ErrDatabaseTimeout: http.StatusGatewayTimeout,
	}

	DefaultAdditionalText = map[string]string{
		ErrDatabaseTimeout: "could not connect to database",
	}
)
