package errors

import (
	"fmt"
	"net/http"
)

type RicardoError interface {
	error
	HttpCode() int
}

type ricardoError struct {
	subject        string
	additionalInfo string
}

func New(subject, additionalInfo string) RicardoError {
	return ricardoError{
		subject:        subject,
		additionalInfo: additionalInfo,
	}
}

func (e ricardoError) Error() string {
	add := e.additionalInfo
	if add == "" {
		dAdd, ok := DefaultAdditionalText[e.subject]
		if !ok {
			add = "no additional info"
		} else {
			add = dAdd
		}
	}

	return fmt.Sprintf("%s: %s", e.subject, add)
}

func (e ricardoError) HttpCode() int {
	code, ok := HttpCodes[e.subject]
	if !ok {
		return http.StatusInternalServerError
	}

	return code
}
