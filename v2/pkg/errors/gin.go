package errors

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
)

// GinErrorOption TODO: add GinErrorOption in the future
type GinErrorOption func (*gin.Context)

func GinErrorHandler(gtx *gin.Context, err error, opts ...GinErrorOption) bool {
	if err != nil {
		for _, opt := range opts {
			opt(gtx)
		}

		_, _ = fmt.Fprintln(os.Stderr, err.Error())
		gtx.JSON(HTTPCode(err), gin.H{"message": err.Error()})
		return true
	}
	return false
}

func GinErrorHandlerWithCode(gtx *gin.Context, err error, code int, opts ...GinErrorOption) bool {
	if err != nil {
		for _, opt := range opts {
			opt(gtx)
		}

		_, _ = fmt.Fprintln(os.Stderr, err.Error())
		gtx.JSON(code, gin.H{"message": err.Error()})
		return true
	}
	return false
}
