package errors

import (
	"errors"
	"net/http"
)

var (
	ErrBadRequest   = errors.New("bad request")
	ErrUnauthorized = errors.New("unauthorized")
	ErrForbidden    = errors.New("forbidden")
	ErrInternal     = errors.New("internal server error")
	ErrTimeout      = errors.New("timeout")
	ErrNotFound     = errors.New("not found")
)

var (
	HTTPCodes = map[error]int{
		ErrBadRequest:   http.StatusBadRequest,
		ErrUnauthorized: http.StatusUnauthorized,
		ErrForbidden:    http.StatusForbidden,
		ErrInternal:     http.StatusInternalServerError,
		ErrTimeout:      http.StatusGatewayTimeout,
		ErrNotFound:     http.StatusNotFound,
	}
)

func HTTPCode(err error) int {
	switch {
	case errors.Is(err, ErrBadRequest):
		return HTTPCodes[ErrBadRequest]
	case errors.Is(err, ErrUnauthorized):
		return HTTPCodes[ErrUnauthorized]
	case errors.Is(err, ErrForbidden):
		return HTTPCodes[ErrForbidden]
	case errors.Is(err, ErrInternal):
		return HTTPCodes[ErrInternal]
	case errors.Is(err, ErrTimeout):
		return HTTPCodes[ErrTimeout]
	case errors.Is(err, ErrNotFound):
		return HTTPCodes[ErrNotFound]
	default:
		return http.StatusInternalServerError
	}
}
